/*
 * Copyright (c) 2014 Red Hat, Inc. and others
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.indev.radex.guimanagement;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link com.indev.radex.guimanagement.Agent}.
 *
 * NOTE: This class has been automatically generated from the {@link com.indev.radex.guimanagement.Agent} original class using Vert.x codegen.
 */
public class AgentConverter {

  public static void fromJson(JsonObject json, Agent obj) {
    if (json.getValue("agentStatus") instanceof String) {
      obj.setAgentStatus(com.indev.radex.guimanagement.AgentStatus.valueOf((String)json.getValue("agentStatus")));
    }
    if (json.getValue("name") instanceof String) {
      obj.setName((String)json.getValue("name"));
    }
  }

  public static void toJson(Agent obj, JsonObject json) {
    if (obj.getAgentStatus() != null) {
      json.put("agentStatus", obj.getAgentStatus().name());
    }
    if (obj.getName() != null) {
      json.put("name", obj.getName());
    }
  }
}