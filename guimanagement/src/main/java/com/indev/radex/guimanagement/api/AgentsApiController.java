package com.indev.radex.guimanagement.api;

import com.indev.radex.agent.AID;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.servicediscovery.ServiceDiscovery;

import static java.lang.String.format;

public class AgentsApiController {

    private AgentsApiService agentsApiService;

    public AgentsApiController(AgentsApiService agentsApiService) {
        this.agentsApiService = agentsApiService;
    }


    public void addOne(RoutingContext routingContext) {
        String agentName = routingContext.getBodyAsJson().getString("name");
        String agentClass = routingContext.getBodyAsJson().getString("class");
        String arguments = routingContext.getBodyAsJson().getString("arguments");

        agentsApiService.add(agentName, agentClass, arguments, agentAsyncResult -> {
            if (agentAsyncResult.succeeded()) {
                routingContext.response().putHeader("content-type", "application/json").setStatusCode(200).end(Json.encodePrettily(agentAsyncResult.result()));
            } else {
                routingContext.response().putHeader("content-type", "application/json").setStatusCode(200).end(Json.encodePrettily("Impossible to add agent " + agentAsyncResult.cause()));
            }
        });

    }


    public void getAll(RoutingContext context, ServiceDiscovery serviceDiscovery) {
        agentsApiService.getAll(agents -> {
            if (agents.succeeded()) {
                context.response()
                        .putHeader("content-type", "application/json")
                        .setStatusCode(200)
                        .end(Json.encodePrettily(agents.result()));
            } else {
                context.response()
                        .setStatusCode(200)
                        .end("Error getting all agents " + agents.cause());
            }
        });

    }

    public void killOne(RoutingContext routingContext) {
        String aid = routingContext.request().getParam("aid");
        if (aid == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            agentsApiService.kill(new AID(aid), voidAsyncResult -> {
                if (voidAsyncResult.succeeded()) {
                    routingContext.response()
                            .putHeader("content-type", "application/json")
                            .setStatusCode(200)
                            .end(Json.encodePrettily(format("Agent %s killed successfully", aid)));
                } else {
                    routingContext.response()
                            .setStatusCode(500)
                            .end("Error killing agent " + voidAsyncResult.cause());
                }
            });

        }
    }
/*
    public void getOne(RoutingContext routingContext) {
        String agentName = routingContext.request().getParam("id");
        if (agentName == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            Agent byName = agentsApiService.getByName(agentName);
            routingContext.response()
                    .putHeader("content-type", "application/json")
                    .setStatusCode(200)
                    .end(Json.encodePrettily(byName));
        }
    }

    public void updateOne(RoutingContext routingContext) {
        String agentName = routingContext.getBodyAsJson().getString("name");
        String action = routingContext.getBodyAsJson().getString("status");

        if (action.equals("kill")) {
            agents.remove(agentName);
        } else if (action.equals("suspend")) {
            agents.get(agentName).setAgentStatus(AgentStatus.SUSPENDED);
        }
    }*/

}
