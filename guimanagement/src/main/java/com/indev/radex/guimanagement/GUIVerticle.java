package com.indev.radex.guimanagement;

import com.indev.radex.agent.lifecycle.AbstractAgent;
import com.indev.radex.guimanagement.api.AgentsApiController;
import com.indev.radex.guimanagement.api.AgentsApiService;
import com.indev.radex.guimanagement.api.DefaultAgentsApiService;
import com.indev.radex.platform.ServiceBuilder;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.servicediscovery.rest.ServiceDiscoveryRestEndpoint;
import io.vertx.servicediscovery.types.HttpEndpoint;

/**
 * Created by elmahdibenzekri on 01/12/2017.
 */
public class GUIVerticle extends AbstractAgent {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private CircuitBreaker circuit;
    private WebClient client;
    private AgentsApiService agentsApiService;
    private HttpServerConfigurator httpServerConfigurator;
    private ServiceBuilder serviceBuilder;
    private AgentsApiController agentsApiController;

    @Override
    protected void initialize(Future initialisationFuture) {
        serviceBuilder = new ServiceBuilder(vertx, serviceDiscovery);
        DefaultAgentsApiService service = new DefaultAgentsApiService(vertx, communication);
        String agentApiServiceAddress = "radex.api.service.agentapi";
        String agentApiServiceName = "agentApiService";
        Class<AgentsApiService> serviceClass = AgentsApiService.class;
        serviceBuilder.createService(agentApiServiceName, agentApiServiceAddress, service, serviceClass, asyncResult -> {
            if (asyncResult.succeeded()) {
                agentsApiController = new AgentsApiController(serviceBuilder.getServiceProxy(agentApiServiceAddress, serviceClass));
                routes(initialisationFuture);
            } else {
                logger.error("Impossible to create agent api service");
                initialisationFuture.fail("Impossible to create agent api service");
            }
        });

    }

    private void routes(Future initialisationFuture) {
        Router router = Router.router(vertx);
        httpServerConfigurator = new HttpServerConfigurator(vertx);
        httpServerConfigurator.configureServer(router, initialisationFuture);

        configureEventBus(router);

        // Discovery endpoint
        ServiceDiscoveryRestEndpoint.create(router, serviceDiscovery);

        // Operations
        router.route("/api/agents*").handler(BodyHandler.create());
        router.post("/api/agents").handler(agentsApiController::addOne);
        router.get("/api/agents").handler(context -> agentsApiController.getAll(context, serviceDiscovery));
        router.delete("/api/agents/:aid").handler(agentsApiController::killOne);
        /*router.get("/api/agents/:id").handler(agentsApiController::getOne);
        router.put("/api/agents/:id").handler(agentsApiController::updateOne);*/

        // Static content
        router.route("/*").handler(StaticHandler.create().setCachingEnabled(false));
    }

    private void configureEventBus(Router router) {
        // Event bus bridge
        SockJSHandler sockJSHandler = SockJSHandler.create(vertx);
        BridgeOptions options = new BridgeOptions();
        options.addOutboundPermitted(new PermittedOptions().setAddress("agents"));

        sockJSHandler.bridge(options);
        router.route("/eventbus/*").handler(sockJSHandler);
    }

    private Future<Void> retrieveDiscoveryService() {
        return Future.future(future -> {
            HttpEndpoint.getWebClient(serviceDiscovery, new JsonObject().put("name", "audit"), client -> {
                this.client = client.result();
                future.handle(client.map((Void) null));
            });
        });
    }

    @Override
    public void stop() throws Exception {
        if (client != null) {
            client.close();
        }
        circuit.close();
    }

}
