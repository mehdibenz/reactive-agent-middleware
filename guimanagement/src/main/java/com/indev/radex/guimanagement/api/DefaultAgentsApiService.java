package com.indev.radex.guimanagement.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.indev.radex.agent.AID;
import com.indev.radex.agent.communication.AgentCommunication;
import com.indev.radex.guimanagement.Agent;
import com.indev.radex.guimanagement.AgentStatus;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DefaultAgentsApiService implements AgentsApiService {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private Map<String, Agent> agents = new HashMap<>();
    private Vertx vertx;
    private AgentCommunication communication;

    public DefaultAgentsApiService(Vertx vertx, AgentCommunication communication) {
        this.vertx = vertx;
        this.communication = communication;
    }

    @Override
    public void getAll(Handler<AsyncResult<List<Agent>>> handler) {
        vertx.eventBus().<String>send("ams.registry", new JsonObject(), messageAsyncResult -> {
            if (messageAsyncResult.succeeded()) {
                TypeReference ref = new TypeReference<List<AID>>() {
                };
                List<AID> aidList = (List) Json.decodeValue(messageAsyncResult.result().body(), ref);

                List<Agent> agents = aidList.stream().map(aid -> new Agent(aid.getName(), AgentStatus.STARTED)).collect(Collectors.toList());
                handler.handle(Future.succeededFuture(agents));
            } else {
                logger.warn("Error when getting all agents from ams " + messageAsyncResult.result());
                handler.handle(Future.succeededFuture(Collections.EMPTY_LIST));
            }
        });
    }

    @Override
    public void add(String name, String agentClass, String arguments, Handler<AsyncResult<Agent>> handler) {
        communication.getAmsService().createAgent(name, agentClass, arguments, actionStatusAsyncResult -> {
            if (actionStatusAsyncResult.succeeded()) {
                Agent agent = new Agent(name, AgentStatus.STARTED);
                agents.put(name, agent);
                handler.handle(Future.succeededFuture(agent));
            } else {
                handler.handle(Future.failedFuture("Impossible to add agent"));
            }
        });
    }


    @Override
    public void kill(AID aid, Handler<AsyncResult<Void>> handler) {
        communication.getAmsService().killAgent(aid, actionStatusAsyncResult -> {
            if (actionStatusAsyncResult.succeeded()) {
                handler.handle(Future.succeededFuture());
            } else {
                handler.handle(Future.failedFuture("Impossible to kill agent " + actionStatusAsyncResult.cause()));
            }
        });
    }
}
