package com.indev.radex.guimanagement;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

/**
 * Created by elmahdibenzekri on 01/12/2017.
 */
@DataObject(generateConverter = true)
public class Agent {
    private String name;
    private AgentStatus agentStatus;

    public Agent(JsonObject jsonObject) {
        AgentConverter.fromJson(jsonObject, this);
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        AgentConverter.toJson(this, json);
        return json;
    }

    public Agent(String name, AgentStatus agentStatus) {
        this.name = name;
        this.agentStatus = agentStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AgentStatus getAgentStatus() {
        return agentStatus;
    }

    public void setAgentStatus(AgentStatus agentStatus) {
        this.agentStatus = agentStatus;
    }
}
