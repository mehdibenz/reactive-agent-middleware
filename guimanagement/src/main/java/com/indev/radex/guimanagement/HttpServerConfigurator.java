package com.indev.radex.guimanagement;

import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CorsHandler;

public class HttpServerConfigurator {
    private Vertx vertx;

    public HttpServerConfigurator(Vertx vertx) {
        this.vertx = vertx;
    }

    public void configureServer(Router router, Future initialisationFuture) {


        router.route().handler(CorsHandler.create("*")
                .allowedMethod(HttpMethod.GET)
                .allowedMethod(HttpMethod.POST)
                .allowedMethod(HttpMethod.OPTIONS)
                .allowedHeader("X-PINGARUNER")
                .allowedHeader("Content-Type"));
        router.get("/probe").handler(ctx -> {

            ctx.response().setChunked(true);

            MultiMap headers = ctx.request().headers();
            for (String key : headers.names()) {
                ctx.response().write(key);
                ctx.response().write(headers.get(key));
                ctx.response().write("\n");
            }

            ctx.response().end();
        });

        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(8181, ar -> {
                    if (ar.failed()) {
                        initialisationFuture.fail(ar.cause());
                    } else {
                        initialisationFuture.complete();
                    }
                });
    }
}
