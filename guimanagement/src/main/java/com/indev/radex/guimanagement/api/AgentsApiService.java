package com.indev.radex.guimanagement.api;

import com.indev.radex.agent.AID;
import com.indev.radex.guimanagement.Agent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

import java.util.List;

@VertxGen
@ProxyGen
public interface AgentsApiService {
    void add(String name, String agentClass, String arguments, Handler<AsyncResult<Agent>> handler);
    void getAll(Handler<AsyncResult<List<Agent>>> handler);
    void kill(AID aid, Handler<AsyncResult<Void>> handler);
}
