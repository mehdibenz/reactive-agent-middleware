package com.indev.radex.guimanagement;

/**
 * Created by elmahdibenzekri on 31/12/2017.
 */
public enum AgentStatus {
    STARTED, SUSPENDED
}
