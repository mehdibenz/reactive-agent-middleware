package com.indev.radex;

import com.indev.radex.agent.AID;
import com.indev.radex.agent.AIDCodec;
import com.indev.radex.directoryservice.operation.ActionStatus;
import com.indev.radex.directoryservice.operation.ActionStatusCodec;
import com.indev.radex.guimanagement.GUIVerticle;
import com.indev.radex.messageservice.message.ACLMessage;
import com.indev.radex.messageservice.message.ACLMessageCodec;
import com.indev.radex.platform.APVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        vertx.eventBus().registerDefaultCodec(ACLMessage.class, new ACLMessageCodec());
        vertx.eventBus().registerDefaultCodec(ActionStatus.class, new ActionStatusCodec());
        vertx.eventBus().registerDefaultCodec(AID.class, new AIDCodec());
        vertx.deployVerticle(new APVerticle(), stringAsyncResult -> {
            vertx.deployVerticle(new GUIVerticle(), new DeploymentOptions().setConfig(new JsonObject().put("AID", "gui")));
        });
    }
}
