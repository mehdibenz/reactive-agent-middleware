/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

/** @module radex-js/message_transport_service */
!function (factory) {
  if (typeof require === 'function' && typeof module !== 'undefined') {
    factory();
  } else if (typeof define === 'function' && define.amd) {
    // AMD loader
    define('radex-js/message_transport_service-proxy', [], factory);
  } else {
    // plain old include
    MessageTransportService = factory();
  }
}(function () {

  /**
 Created by elmahdibenzekri on 15/07/2017.

 @class
  */
  var MessageTransportService = function(eb, address) {

    var j_eb = eb;
    var j_address = address;
    var closed = false;
    var that = this;
    var convCharCollection = function(coll) {
      var ret = [];
      for (var i = 0;i < coll.length;i++) {
        ret.push(String.fromCharCode(coll[i]));
      }
      return ret;
    };

    /**

     @public
     @param transportDescription {Object} 
     @param resultHandler {function} 
     */
    this.bind = function(transportDescription, resultHandler) {
      var __args = arguments;
      if (__args.length === 2 && (typeof __args[0] === 'object' && __args[0] != null) && typeof __args[1] === 'function') {
        if (closed) {
          throw new Error('Proxy is closed');
        }
        j_eb.send(j_address, {"transportDescription":__args[0]}, {"action":"bind"}, function(err, result) { __args[1](err, result &&result.body); });
        return;
      } else throw new TypeError('function invoked with invalid arguments');
    };

    /**

     @public
     @param transportDescription {Object} 
     @param resultHandler {function} 
     */
    this.unbind = function(transportDescription, resultHandler) {
      var __args = arguments;
      if (__args.length === 2 && (typeof __args[0] === 'object' && __args[0] != null) && typeof __args[1] === 'function') {
        if (closed) {
          throw new Error('Proxy is closed');
        }
        j_eb.send(j_address, {"transportDescription":__args[0]}, {"action":"unbind"}, function(err, result) { __args[1](err, result &&result.body); });
        return;
      } else throw new TypeError('function invoked with invalid arguments');
    };

    /**

     @public
     @param ACLMessage {Object} 
     */
    this.publish = function(ACLMessage) {
      var __args = arguments;
      if (__args.length === 1 && (typeof __args[0] === 'object' && __args[0] != null)) {
        if (closed) {
          throw new Error('Proxy is closed');
        }
        j_eb.send(j_address, {"ACLMessage":__args[0]}, {"action":"publish"});
        return;
      } else throw new TypeError('function invoked with invalid arguments');
    };

    /**

     @public
     @param ACLMessage {Object} 
     @param handler {function} 
     */
    this.send = function(ACLMessage, handler) {
      var __args = arguments;
      if (__args.length === 2 && (typeof __args[0] === 'object' && __args[0] != null) && typeof __args[1] === 'function') {
        if (closed) {
          throw new Error('Proxy is closed');
        }
        j_eb.send(j_address, {"ACLMessage":__args[0]}, {"action":"send"}, function(err, result) { __args[1](err, result &&result.body); });
        return;
      } else throw new TypeError('function invoked with invalid arguments');
    };

  };

  if (typeof exports !== 'undefined') {
    if (typeof module !== 'undefined' && module.exports) {
      exports = module.exports = MessageTransportService;
    } else {
      exports.MessageTransportService = MessageTransportService;
    }
  } else {
    return MessageTransportService;
  }
});