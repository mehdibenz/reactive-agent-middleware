/*
 * Copyright (c) 2014 Red Hat, Inc. and others
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.indev.radex.directoryservice;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link com.indev.radex.directoryservice.ServiceDescription}.
 *
 * NOTE: This class has been automatically generated from the {@link com.indev.radex.directoryservice.ServiceDescription} original class using Vert.x codegen.
 */
public class ServiceDescriptionConverter {

  public static void fromJson(JsonObject json, ServiceDescription obj) {
    if (json.getValue("languages") instanceof JsonArray) {
      java.util.LinkedHashSet<java.lang.String> list = new java.util.LinkedHashSet<>();
      json.getJsonArray("languages").forEach( item -> {
        if (item instanceof String)
          list.add((String)item);
      });
      obj.setLanguages(list);
    }
    if (json.getValue("name") instanceof String) {
      obj.setName((String)json.getValue("name"));
    }
    if (json.getValue("ontologies") instanceof JsonArray) {
      java.util.LinkedHashSet<java.lang.String> list = new java.util.LinkedHashSet<>();
      json.getJsonArray("ontologies").forEach( item -> {
        if (item instanceof String)
          list.add((String)item);
      });
      obj.setOntologies(list);
    }
    if (json.getValue("ownership") instanceof String) {
      obj.setOwnership((String)json.getValue("ownership"));
    }
    if (json.getValue("properties") instanceof JsonArray) {
      java.util.LinkedHashSet<java.lang.String> list = new java.util.LinkedHashSet<>();
      json.getJsonArray("properties").forEach( item -> {
        if (item instanceof String)
          list.add((String)item);
      });
      obj.setProperties(list);
    }
    if (json.getValue("protocols") instanceof JsonArray) {
      java.util.LinkedHashSet<java.lang.String> list = new java.util.LinkedHashSet<>();
      json.getJsonArray("protocols").forEach( item -> {
        if (item instanceof String)
          list.add((String)item);
      });
      obj.setProtocols(list);
    }
    if (json.getValue("type") instanceof String) {
      obj.setType((String)json.getValue("type"));
    }
  }

  public static void toJson(ServiceDescription obj, JsonObject json) {
    if (obj.getLanguages() != null) {
      JsonArray array = new JsonArray();
      obj.getLanguages().forEach(item -> array.add(item));
      json.put("languages", array);
    }
    if (obj.getName() != null) {
      json.put("name", obj.getName());
    }
    if (obj.getOntologies() != null) {
      JsonArray array = new JsonArray();
      obj.getOntologies().forEach(item -> array.add(item));
      json.put("ontologies", array);
    }
    if (obj.getOwnership() != null) {
      json.put("ownership", obj.getOwnership());
    }
    if (obj.getProperties() != null) {
      JsonArray array = new JsonArray();
      obj.getProperties().forEach(item -> array.add(item));
      json.put("properties", array);
    }
    if (obj.getProtocols() != null) {
      JsonArray array = new JsonArray();
      obj.getProtocols().forEach(item -> array.add(item));
      json.put("protocols", array);
    }
    if (obj.getType() != null) {
      json.put("type", obj.getType());
    }
  }
}