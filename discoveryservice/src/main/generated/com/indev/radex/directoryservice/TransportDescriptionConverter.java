/*
 * Copyright (c) 2014 Red Hat, Inc. and others
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.indev.radex.directoryservice;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link com.indev.radex.directoryservice.TransportDescription}.
 *
 * NOTE: This class has been automatically generated from the {@link com.indev.radex.directoryservice.TransportDescription} original class using Vert.x codegen.
 */
public class TransportDescriptionConverter {

  public static void fromJson(JsonObject json, TransportDescription obj) {
    if (json.getValue("transportSpecificAddress") instanceof String) {
      obj.setTransportSpecificAddress((String)json.getValue("transportSpecificAddress"));
    }
    if (json.getValue("transportSpecificProperties") instanceof JsonObject) {
      java.util.Map<String, java.lang.String> map = new java.util.LinkedHashMap<>();
      json.getJsonObject("transportSpecificProperties").forEach(entry -> {
        if (entry.getValue() instanceof String)
          map.put(entry.getKey(), (String)entry.getValue());
      });
      obj.setTransportSpecificProperties(map);
    }
    if (json.getValue("transportType") instanceof String) {
      obj.setTransportType(com.indev.radex.directoryservice.TransportType.valueOf((String)json.getValue("transportType")));
    }
  }

  public static void toJson(TransportDescription obj, JsonObject json) {
    if (obj.getTransportSpecificAddress() != null) {
      json.put("transportSpecificAddress", obj.getTransportSpecificAddress());
    }
    if (obj.getTransportSpecificProperties() != null) {
      JsonObject map = new JsonObject();
      obj.getTransportSpecificProperties().forEach((key,value) -> map.put(key, value));
      json.put("transportSpecificProperties", map);
    }
    if (obj.getTransportType() != null) {
      json.put("transportType", obj.getTransportType().name());
    }
  }
}