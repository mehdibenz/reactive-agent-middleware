/*
 * Copyright (c) 2014 Red Hat, Inc. and others
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.indev.radex.directoryservice.service;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link com.indev.radex.directoryservice.service.DFDescription}.
 *
 * NOTE: This class has been automatically generated from the {@link com.indev.radex.directoryservice.service.DFDescription} original class using Vert.x codegen.
 */
public class DFDescriptionConverter {

  public static void fromJson(JsonObject json, DFDescription obj) {
    if (json.getValue("languages") instanceof JsonArray) {
      java.util.LinkedHashSet<java.lang.String> list = new java.util.LinkedHashSet<>();
      json.getJsonArray("languages").forEach( item -> {
        if (item instanceof String)
          list.add((String)item);
      });
      obj.setLanguages(list);
    }
    if (json.getValue("name") instanceof JsonObject) {
      obj.setName(new com.indev.radex.agent.AID((JsonObject)json.getValue("name")));
    }
    if (json.getValue("ontologies") instanceof JsonArray) {
      java.util.LinkedHashSet<java.lang.String> list = new java.util.LinkedHashSet<>();
      json.getJsonArray("ontologies").forEach( item -> {
        if (item instanceof String)
          list.add((String)item);
      });
      obj.setOntologies(list);
    }
    if (json.getValue("protocols") instanceof JsonArray) {
      java.util.LinkedHashSet<java.lang.String> list = new java.util.LinkedHashSet<>();
      json.getJsonArray("protocols").forEach( item -> {
        if (item instanceof String)
          list.add((String)item);
      });
      obj.setProtocols(list);
    }
    if (json.getValue("services") instanceof JsonArray) {
      java.util.LinkedHashSet<com.indev.radex.directoryservice.ServiceDescription> list = new java.util.LinkedHashSet<>();
      json.getJsonArray("services").forEach( item -> {
        if (item instanceof JsonObject)
          list.add(new com.indev.radex.directoryservice.ServiceDescription((JsonObject)item));
      });
      obj.setServices(list);
    }
  }

  public static void toJson(DFDescription obj, JsonObject json) {
    if (obj.getLanguages() != null) {
      JsonArray array = new JsonArray();
      obj.getLanguages().forEach(item -> array.add(item));
      json.put("languages", array);
    }
    if (obj.getName() != null) {
      json.put("name", obj.getName().toJson());
    }
    if (obj.getOntologies() != null) {
      JsonArray array = new JsonArray();
      obj.getOntologies().forEach(item -> array.add(item));
      json.put("ontologies", array);
    }
    if (obj.getProtocols() != null) {
      JsonArray array = new JsonArray();
      obj.getProtocols().forEach(item -> array.add(item));
      json.put("protocols", array);
    }
    if (obj.getServices() != null) {
      JsonArray array = new JsonArray();
      obj.getServices().forEach(item -> array.add(item.toJson()));
      json.put("services", array);
    }
  }
}