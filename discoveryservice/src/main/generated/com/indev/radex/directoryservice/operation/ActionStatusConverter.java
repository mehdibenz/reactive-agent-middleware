/*
 * Copyright (c) 2014 Red Hat, Inc. and others
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.indev.radex.directoryservice.operation;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link com.indev.radex.directoryservice.operation.ActionStatus}.
 *
 * NOTE: This class has been automatically generated from the {@link com.indev.radex.directoryservice.operation.ActionStatus} original class using Vert.x codegen.
 */
public class ActionStatusConverter {

  public static void fromJson(JsonObject json, ActionStatus obj) {
    if (json.getValue("code") instanceof String) {
      obj.setCode((String)json.getValue("code"));
    }
    if (json.getValue("entries") instanceof JsonArray) {
      java.util.LinkedHashSet<com.indev.radex.directoryservice.service.DFDescription> list = new java.util.LinkedHashSet<>();
      json.getJsonArray("entries").forEach( item -> {
        if (item instanceof JsonObject)
          list.add(new com.indev.radex.directoryservice.service.DFDescription((JsonObject)item));
      });
      obj.setEntries(list);
    }
    if (json.getValue("success") instanceof Boolean) {
      obj.setSuccess((Boolean)json.getValue("success"));
    }
  }

  public static void toJson(ActionStatus obj, JsonObject json) {
    if (obj.getCause() != null) {
      json.put("cause", obj.getCause());
    }
    if (obj.getCode() != null) {
      json.put("code", obj.getCode());
    }
    if (obj.getEntries() != null) {
      JsonArray array = new JsonArray();
      obj.getEntries().forEach(item -> array.add(item.toJson()));
      json.put("entries", array);
    }
    json.put("success", obj.isSuccess());
  }
}