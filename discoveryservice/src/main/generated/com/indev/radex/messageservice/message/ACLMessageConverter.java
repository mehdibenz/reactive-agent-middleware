/*
 * Copyright (c) 2014 Red Hat, Inc. and others
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.indev.radex.messageservice.message;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link com.indev.radex.messageservice.message.ACLMessage}.
 *
 * NOTE: This class has been automatically generated from the {@link com.indev.radex.messageservice.message.ACLMessage} original class using Vert.x codegen.
 */
public class ACLMessageConverter {

  public static void fromJson(JsonObject json, ACLMessage obj) {
    if (json.getValue("conversationId") instanceof String) {
      obj.setConversationId((String)json.getValue("conversationId"));
    }
    if (json.getValue("inReplyTo") instanceof String) {
      obj.setInReplyTo((String)json.getValue("inReplyTo"));
    }
    if (json.getValue("language") instanceof String) {
      obj.setLanguage((String)json.getValue("language"));
    }
    if (json.getValue("payload") instanceof String) {
      obj.setPayload((String)json.getValue("payload"));
    }
    if (json.getValue("performative") instanceof String) {
      obj.setPerformative(com.indev.radex.messageservice.Performative.valueOf((String)json.getValue("performative")));
    }
    if (json.getValue("receivers") instanceof JsonArray) {
      java.util.ArrayList<com.indev.radex.agent.AID> list = new java.util.ArrayList<>();
      json.getJsonArray("receivers").forEach( item -> {
        if (item instanceof JsonObject)
          list.add(new com.indev.radex.agent.AID((JsonObject)item));
      });
      obj.setReceivers(list);
    }
    if (json.getValue("replyWith") instanceof String) {
      obj.setReplyWith((String)json.getValue("replyWith"));
    }
    if (json.getValue("sender") instanceof JsonObject) {
      obj.setSender(new com.indev.radex.agent.AID((JsonObject)json.getValue("sender")));
    }
  }

  public static void toJson(ACLMessage obj, JsonObject json) {
    if (obj.getConversationId() != null) {
      json.put("conversationId", obj.getConversationId());
    }
    if (obj.getInReplyTo() != null) {
      json.put("inReplyTo", obj.getInReplyTo());
    }
    if (obj.getLanguage() != null) {
      json.put("language", obj.getLanguage());
    }
    if (obj.getPayload() != null) {
      json.put("payload", obj.getPayload());
    }
    if (obj.getPerformative() != null) {
      json.put("performative", obj.getPerformative().name());
    }
    if (obj.getReceivers() != null) {
      JsonArray array = new JsonArray();
      obj.getReceivers().forEach(item -> array.add(item.toJson()));
      json.put("receivers", array);
    }
    if (obj.getReplyWith() != null) {
      json.put("replyWith", obj.getReplyWith());
    }
    if (obj.getSender() != null) {
      json.put("sender", obj.getSender().toJson());
    }
  }
}