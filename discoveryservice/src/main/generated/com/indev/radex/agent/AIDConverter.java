/*
 * Copyright (c) 2014 Red Hat, Inc. and others
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.indev.radex.agent;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link com.indev.radex.agent.AID}.
 *
 * NOTE: This class has been automatically generated from the {@link com.indev.radex.agent.AID} original class using Vert.x codegen.
 */
public class AIDConverter {

  public static void fromJson(JsonObject json, AID obj) {
    if (json.getValue("addresses") instanceof JsonArray) {
      java.util.ArrayList<com.indev.radex.directoryservice.TransportDescription> list = new java.util.ArrayList<>();
      json.getJsonArray("addresses").forEach( item -> {
        if (item instanceof JsonObject)
          list.add(new com.indev.radex.directoryservice.TransportDescription((JsonObject)item));
      });
      obj.setAddresses(list);
    }
    if (json.getValue("name") instanceof String) {
      obj.setName((String)json.getValue("name"));
    }
    if (json.getValue("resolvers") instanceof JsonArray) {
      java.util.LinkedHashSet<java.lang.String> list = new java.util.LinkedHashSet<>();
      json.getJsonArray("resolvers").forEach( item -> {
        if (item instanceof String)
          list.add((String)item);
      });
      obj.setResolvers(list);
    }
  }

  public static void toJson(AID obj, JsonObject json) {
    if (obj.getAddresses() != null) {
      JsonArray array = new JsonArray();
      obj.getAddresses().forEach(item -> array.add(item.toJson()));
      json.put("addresses", array);
    }
    if (obj.getName() != null) {
      json.put("name", obj.getName());
    }
    if (obj.getResolvers() != null) {
      JsonArray array = new JsonArray();
      obj.getResolvers().forEach(item -> array.add(item));
      json.put("resolvers", array);
    }
  }
}