package com.indev.radex.messageservice.message;

public class ACLMessageReplyBuilder {

    public ACLMessage build(ACLMessage aclMessage) {
        ACLMessage m = new ACLMessage(aclMessage.getPerformative());

        m.setInReplyTo(aclMessage.getReplyWith());
        if (aclMessage.getSender()!= null) {
            m.setReplyWith(aclMessage.getSender().getName() + java.lang.System.currentTimeMillis());
            m.addReceiver(aclMessage.getSender());
        }
        else
            m.setReplyWith("X" + java.lang.System.currentTimeMillis());
        m.setConversationId(aclMessage.getConversationId());


        return m;
    }
}
