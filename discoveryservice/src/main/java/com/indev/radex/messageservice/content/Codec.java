package com.indev.radex.messageservice.content;

import com.indev.radex.messageservice.message.SLAction;

public interface Codec {
    SLAction decode(String payload);

    String encode(SLAction action);
}
