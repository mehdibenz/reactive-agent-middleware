package com.indev.radex.messageservice;

/**
 * Created by elmahdibenzekri on 27/06/2017.
 */
public enum Performative {
    ACCEPT_PROPOSAL,
    AGREE,
    CANCEL,
    CFP,
    CONFIRM,
    DISCONFIRM,
    FAILURE,
    INFORM,
    INFORM_IF,
    INFORM_REF,
    NOT_UNDERSTOOD,
    PROPOSE,
    QUERY_IF,
    QUERY_REF,
    REFUSE,
    REJECT_PROPOSAL,
    REQUEST,
    REQUEST_WHEN,
    REQUEST_WHENEVER,
    SUBSCRIBE,
    PROXY,
    PROPAGATE,
    UNKNOWN
}
