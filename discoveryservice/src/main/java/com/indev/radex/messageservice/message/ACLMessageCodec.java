package com.indev.radex.messageservice.message;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class ACLMessageCodec implements MessageCodec<ACLMessage, ACLMessage> {
    @Override
    public void encodeToWire(Buffer buffer, ACLMessage aclMessage) {
        final JsonObject jsonToEncode = new JsonObject();
        jsonToEncode.put("performative", aclMessage.getPerformative());
        jsonToEncode.put("payload", aclMessage.getPayload());
        jsonToEncode.put("conversationId", aclMessage.getConversationId());
        jsonToEncode.put("replyWith", aclMessage.getReplyWith());
        jsonToEncode.put("inReplyTo", aclMessage.getInReplyTo());
        JsonArray receivers = new JsonArray();
        aclMessage.getReceivers().forEach(aid -> {
            JsonObject receiver = new JsonObject().put("name", aid.getName());
            JsonArray addresses = new JsonArray();
            aid.getAddresses().forEach(transportDescription -> {
                addresses.add(transportDescription.getTransportSpecificAddress());
            });
            receiver.put("addresses", addresses);
            receivers.add(receiver);
        });
        jsonToEncode.put("receivers", receivers);

        // Encode object to string
        final String jsonToStr = jsonToEncode.encode();

        // Length of JSON: is NOT characters count
        final int length = jsonToStr.getBytes().length;

        // Write data into given buffer
        buffer.appendInt(length);
        buffer.appendString(jsonToStr);
    }

    @Override
    public ACLMessage decodeFromWire(int i, Buffer buffer) {
        // My custom message starting from this *position* of buffer
        int _pos = i;

        // Length of JSON
        final int length = buffer.getInt(_pos);

        // Get JSON string by it`s length
        // Jump 4 because getInt() == 4 bytes
        final String jsonStr = buffer.getString(_pos += 4, _pos + length);
        final JsonObject contentJson = new JsonObject(jsonStr);

        return new ACLMessage(contentJson);
    }

    @Override
    public ACLMessage transform(ACLMessage aclMessage) {
        return aclMessage;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
