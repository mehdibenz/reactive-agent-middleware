package com.indev.radex.messageservice;

import com.indev.radex.directoryservice.TransportDescription;
import com.indev.radex.messageservice.message.ACLMessage;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

/**
 * Created by elmahdibenzekri on 15/07/2017.
 */
@VertxGen
@ProxyGen
public interface MessageTransportService {
    String ADDRESS = "radex.service.messagetransport";

    void bind(TransportDescription transportDescription, Handler<AsyncResult<ACLMessage>> resultHandler);

    void unbind(TransportDescription transportDescription, Handler<AsyncResult<ACLMessage>> resultHandler);

    void publish(ACLMessage ACLMessage);

    void send(ACLMessage ACLMessage, Handler<AsyncResult<ACLMessage>> handler);

}
