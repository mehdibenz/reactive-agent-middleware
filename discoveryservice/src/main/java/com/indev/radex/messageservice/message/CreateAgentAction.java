package com.indev.radex.messageservice.message;

import com.indev.radex.ams.operation.AMSOperation;
import com.indev.radex.ams.operation.AMSCreateAgentOperation;

public class CreateAgentAction extends SLAction {
    public String agentName;
    public String agentClass;
    public String arguments;

    public CreateAgentAction(String agentName, String agentClass, String arguments) {
        this.agentName = agentName;
        this.agentClass = agentClass;
        this.arguments = arguments;
    }

    @Override
    public AMSOperation toAMSOperation() {
        return new AMSCreateAgentOperation(agentName, agentClass, arguments);
    }
}
