package com.indev.radex.messageservice.message;

import com.indev.radex.agent.AID;
import com.indev.radex.messageservice.Performative;
import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elmahdibenzekri on 11/06/2017.
 */
@DataObject(generateConverter = true)
public class ACLMessage {
    private Performative performative;
    private AID sender;
    private List<AID> receivers;
    private String payload = "";
    private Object objectPayload;
    private String conversationId = "";
    private String replyWith = "";
    private String inReplyTo = "";
    private String language = "";

    public ACLMessage() {
    }

    public ACLMessage(Performative performative) {
        this.performative = performative;
    }

    public ACLMessage(JsonObject jsonObject) {
        ACLMessageConverter.fromJson(jsonObject, this);
    }

    public JsonObject toJson() {
        JsonObject jsonObject = new JsonObject();
        ACLMessageConverter.toJson(this, jsonObject);
        return jsonObject;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public Performative getPerformative() {
        return performative;
    }

    public void setPerformative(Performative performative) {
        this.performative = performative;
    }

    public AID getSender() {
        return sender;
    }

    public void setSender(AID sender) {
        this.sender = sender;
    }

    public List<AID> getReceivers() {
        if (receivers == null) {
            receivers = new ArrayList<>();
        }
        return receivers;
    }

    public void setReceivers(List<AID> receivers) {
        this.receivers = receivers;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getReplyWith() {
        return replyWith;
    }

    public void setReplyWith(String replyWith) {
        this.replyWith = replyWith;
    }

    public String getInReplyTo() {
        return inReplyTo;
    }

    public void setInReplyTo(String inReplyTo) {
        this.inReplyTo = inReplyTo;
    }

    public boolean addReceiver(AID aid) {
        return getReceivers().add(aid);
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Object getObjectPayload() {
        return objectPayload;
    }

    public void setObjectPayload(Object objectPayload) {
        this.objectPayload = objectPayload;
    }

    public ACLMessage createReply() {
        return new ACLMessageReplyBuilder().build(this);
    }
}
