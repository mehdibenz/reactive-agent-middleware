package com.indev.radex.messageservice.message;

import com.indev.radex.ams.operation.AMSOperation;

public abstract class SLAction {
    public abstract AMSOperation toAMSOperation();
}
