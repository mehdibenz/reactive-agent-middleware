package com.indev.radex.messageservice;

import com.indev.radex.agent.AID;
import com.indev.radex.directoryservice.TransportDescription;
import com.indev.radex.messageservice.message.ACLMessage;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;

/**
 * Created by elmahdibenzekri on 11/06/2017.
 */
public class DefaultMessageTransportService implements MessageTransportService {
    private final Vertx vertx;

    public DefaultMessageTransportService(Vertx vertx) {
        this.vertx = vertx;
    }


    public void bind(TransportDescription transportDescription, Handler<AsyncResult<ACLMessage>> resultHandler) {
        vertx.eventBus().consumer(transportDescription.getTransportSpecificAddress(), message -> {
            resultHandler.handle(Future.succeededFuture(new ACLMessage()));
        });
    }

    public void unbind(TransportDescription transportDescription, Handler<AsyncResult<ACLMessage>> resultHandler) {
        vertx.eventBus().consumer(transportDescription.getTransportSpecificAddress()).unregister();
        resultHandler.handle(Future.succeededFuture());
    }

    public void publish(ACLMessage aclMessage) {
        for (AID receiver : aclMessage.getReceivers()) {
            vertx.eventBus().publish("agent." + receiver.getName(), aclMessage);//todo use transport address
        }
    }

    public void send(ACLMessage aclMessage, Handler<AsyncResult<ACLMessage>> handler) {
        for (AID receiver : aclMessage.getReceivers()) {
            vertx.eventBus().<ACLMessage>send("agent." + receiver.getName(), aclMessage, messageAsyncResult -> {
                if(messageAsyncResult.succeeded()){
                    handler.handle(Future.succeededFuture(messageAsyncResult.result().body()));
                }
            });//todo use transport address
        }
    }
}
