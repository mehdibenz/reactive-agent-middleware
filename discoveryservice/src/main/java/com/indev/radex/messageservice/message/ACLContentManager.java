package com.indev.radex.messageservice.message;

import com.indev.radex.messageservice.content.Codec;
import com.indev.radex.messageservice.content.TextCodec;

import java.util.HashMap;
import java.util.Map;

public class ACLContentManager {
    private Map<String, Codec> codecs;

    public ACLContentManager() {
        codecs = new HashMap<>();
        codecs.put("text", new TextCodec());
    }


    public SLAction toAction(ACLMessage aclMessage) {
        if (codecs.containsKey(aclMessage.getLanguage())) {
            String payload = aclMessage.getPayload();
            SLAction action = codecs.get(aclMessage.getLanguage()).decode(payload);
            return action;
        }
        throw new IllegalStateException("Unsupported language");
    }
}
