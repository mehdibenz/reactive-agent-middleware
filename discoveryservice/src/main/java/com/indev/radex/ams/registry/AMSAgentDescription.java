package com.indev.radex.ams.registry;

import com.indev.radex.agent.AID;
import com.indev.radex.ams.AgentLifecycleState;

/**
 * Created by elmahdibenzekri on 05/11/2017.
 */
public class AMSAgentDescription {
    private AID name;
    private AgentLifecycleState state;
    private String deploymentId;

    public AMSAgentDescription(AID name) {
        this.name = name;
    }

    public AMSAgentDescription(AID name, AgentLifecycleState state) {
        this.name = name;
        this.state = state;
    }

    public AID getName() {
        return name;
    }

    public void setName(AID name) {
        this.name = name;
    }

    public AgentLifecycleState getState() {
        return state;
    }

    public void setState(AgentLifecycleState state) {
        this.state = state;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }
}
