package com.indev.radex.ams;

/**
 * Created by elmahdibenzekri on 05/11/2017.
 */
public enum AgentLifecycleState {
    INITIATED,
    ACTIVE,
    SUSPENDED,
    WAITING,
    TRANSIT,
}
