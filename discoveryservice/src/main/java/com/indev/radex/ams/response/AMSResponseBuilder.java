package com.indev.radex.ams.response;

import com.indev.radex.directoryservice.operation.ActionStatus;
import com.indev.radex.messageservice.Performative;
import com.indev.radex.messageservice.message.ACLMessage;
import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.Message;

public class AMSResponseBuilder {

    public void replyTo(AsyncResult asyncResult, Message<ACLMessage> aclMessageMessage) {
        ACLMessage responseMessage = new ACLMessage();
        responseMessage.setPerformative(Performative.INFORM);
        if (asyncResult.succeeded()) {
            responseMessage.setObjectPayload(new ActionStatus(true));
        } else {
            responseMessage.setObjectPayload(new ActionStatus(false).withCause(asyncResult.cause().getMessage()));
        }
        aclMessageMessage.reply(responseMessage);
    }
}
