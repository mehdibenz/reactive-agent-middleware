package com.indev.radex.ams;

import com.indev.radex.agent.AID;
import com.indev.radex.agent.lifecycle.AbstractAgent;
import com.indev.radex.ams.operation.AMSOperation;
import com.indev.radex.ams.operation.AMSOperationFactory;
import com.indev.radex.ams.registry.AMSAgentDescription;
import com.indev.radex.ams.registry.AMSAgentRegistry;
import com.indev.radex.ams.response.AMSResponseBuilder;
import com.indev.radex.directoryservice.operation.ActionStatus;
import com.indev.radex.messageservice.Performative;
import com.indev.radex.messageservice.message.ACLMessage;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.Json;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elmahdibenzekri on 05/11/2017.
 */
public class AMS extends AbstractAgent {

    private AMSAgentRegistry registry;
    private AgentPlatformDescription platformDescription;
    private AMSOperationFactory amsOperationFactory;
    private AMSResponseBuilder amsResponseBuilder;

    @Override
    protected void initialize(Future initialisationFuture) {
        registry = new AMSAgentRegistry();
        amsOperationFactory = new AMSOperationFactory();
        amsResponseBuilder = new AMSResponseBuilder();

        communication.onMessageReceived(aclMessageMessage -> {
            AMSOperation amsOperation = amsOperationFactory.from(aclMessageMessage.body());
            amsOperation.execute(vertx, registry, voidAsyncResult -> {
                amsResponseBuilder.replyTo(voidAsyncResult, aclMessageMessage);
            });
        });

        vertx.eventBus().consumer("ams.registry", message -> {
            List<AID> aidList = new ArrayList(registry.getAgentDirectoryEntries().keySet());

            message.reply(Json.encode(aidList));
        });
        initialisationFuture.complete();
    }

    public void register(AMSAgentDescription agentDescription, Handler<AsyncResult<ActionStatus>> resultHandler) {
        registry.register(agentDescription);
        resultHandler.handle(Future.succeededFuture(new ActionStatus(true)));
    }

    public void quitAgent(AMSAgentDescription agentDescription) {
        AID agentId = registry.search(agentDescription);

        ACLMessage quitMessage = new ACLMessage();
        quitMessage.addReceiver(agentId);
        quitMessage.setPerformative(Performative.REQUEST);
        quitMessage.setPayload("quit");
        communication.send(quitMessage);
    }

    public void destroyAgent(AMSAgentDescription agentDescription) {
        AID agentId = registry.search(agentDescription);

        ACLMessage quitMessage = new ACLMessage();
        quitMessage.addReceiver(agentId);
        quitMessage.setPerformative(Performative.REQUEST);
        quitMessage.setPayload("quit");
        communication.send(quitMessage);
    }

    public void suspendAgent(AMSAgentDescription agentDescription) {
        AID agentId = registry.search(agentDescription);

        ACLMessage quitMessage = new ACLMessage();
        quitMessage.addReceiver(agentId);
        quitMessage.setPerformative(Performative.REQUEST);
        quitMessage.setPayload("quit");
        communication.send(quitMessage);
    }

    public void resumeAgent(AMSAgentDescription agentDescription) {
        AID agentId = registry.search(agentDescription);

        ACLMessage quitMessage = new ACLMessage();
        quitMessage.addReceiver(agentId);
        quitMessage.setPerformative(Performative.REQUEST);
        quitMessage.setPayload("quit");
        communication.send(quitMessage);
    }

    public void modify(AMSAgentDescription agentDescription) {
        registry.modify(agentDescription);
    }

    public AID search(AMSAgentDescription agentDescription) {
        return registry.search(agentDescription);
    }
}
