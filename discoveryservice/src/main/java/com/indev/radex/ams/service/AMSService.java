package com.indev.radex.ams.service;


import com.indev.radex.agent.AID;
import com.indev.radex.directoryservice.operation.ActionStatus;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

@VertxGen
@ProxyGen
public interface AMSService {
    String ADDRESS = "radex.service.ams";

    void createAgent(String name, String agentClass, String agentName, Handler<AsyncResult<ActionStatus>> handler);
    void killAgent(AID aid, Handler<AsyncResult<ActionStatus>> handler);
}
