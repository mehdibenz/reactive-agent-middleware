package com.indev.radex.ams.operation;

import com.indev.radex.agent.AID;
import com.indev.radex.ams.registry.AMSAgentDescription;
import com.indev.radex.ams.registry.AMSAgentRegistry;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;

public class AMSCreateAgentOperation extends AMSOperation {
    private String agentName;
    private String className;
    private String arguments;

    public AMSCreateAgentOperation(String agentName, String className, String arguments) {
        this.agentName = agentName;
        this.className = className;
        this.arguments = arguments;
    }

    @Override
    public void execute(Vertx vertx, AMSAgentRegistry registry, Handler<AsyncResult<Void>> handler) {
        DeploymentOptions deploymentOptions = new DeploymentOptions();
        String aid = agentName + "@platform1";
        deploymentOptions.setConfig(new JsonObject().put("AID", aid));
        vertx.deployVerticle(className, deploymentOptions, stringAsyncResult -> {
            if (stringAsyncResult.succeeded()) {
                AMSAgentDescription agentDescription = new AMSAgentDescription(new AID(aid));
                agentDescription.setDeploymentId(stringAsyncResult.result());
                registry.register(agentDescription);
                handler.handle(Future.succeededFuture());
            } else {
                logger.error("Unexpected error while deploying verticle " + aid + " class name " + className);
                handler.handle(Future.failedFuture(stringAsyncResult.cause()));
            }
        });
    }
}
