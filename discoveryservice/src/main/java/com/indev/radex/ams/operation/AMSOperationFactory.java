package com.indev.radex.ams.operation;

import com.indev.radex.messageservice.message.ACLMessage;

public class AMSOperationFactory {

    public AMSOperation from(ACLMessage aclMessage) {
        if (aclMessage.getObjectPayload() instanceof AMSOperation) {
            return (AMSOperation) aclMessage.getObjectPayload();
        }
        throw new IllegalArgumentException("ACL message doesn't contain ams operation " + aclMessage);
        /*ACLContentManager aclContentMAnager = new ACLContentManager();
        SLAction slAction = aclContentMAnager.toAction(aclMessage);
        return slAction.toAMSOperation();*/
    }
}
