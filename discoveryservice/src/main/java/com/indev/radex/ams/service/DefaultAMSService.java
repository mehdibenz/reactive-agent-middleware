package com.indev.radex.ams.service;

import com.indev.radex.agent.AID;
import com.indev.radex.ams.operation.AMSCreateAgentOperation;
import com.indev.radex.ams.operation.AMSKillAgentOperation;
import com.indev.radex.directoryservice.operation.ActionStatus;
import com.indev.radex.messageservice.MessageTransportService;
import com.indev.radex.messageservice.Performative;
import com.indev.radex.messageservice.message.ACLMessage;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;

public class DefaultAMSService implements AMSService {
    private MessageTransportService messageTransportService;
    private AID amsAID;

    public DefaultAMSService(MessageTransportService messageTransportService, AID amsAID) {
        this.messageTransportService = messageTransportService;
        this.amsAID = amsAID;
    }

    @Override
    public void createAgent(String name, String agentClass, String arguments, Handler<AsyncResult<ActionStatus>> handler) {
        ACLMessage message = new ACLMessage();
        message.addReceiver(amsAID);
        message.setPerformative(Performative.REQUEST);

        AMSCreateAgentOperation createAgentOperation = new AMSCreateAgentOperation(name, agentClass, arguments);
        message.setObjectPayload(createAgentOperation);
        messageTransportService.send(message, messageAsyncResult -> {
            if (messageAsyncResult.succeeded()) {
                ACLMessage responseMessage = messageAsyncResult.result();
                handler.handle(Future.succeededFuture(ActionStatus.SUCCESS));
            } else {
                handler.handle(Future.failedFuture("Impossible to send message createAgentOperation "));
            }
        });
    }

    @Override
    public void killAgent(AID aid, Handler<AsyncResult<ActionStatus>> handler) {
        ACLMessage message = new ACLMessage();
        message.addReceiver(amsAID);
        message.setPerformative(Performative.REQUEST);

        AMSKillAgentOperation killAgentOperation = new AMSKillAgentOperation(aid);
        message.setObjectPayload(killAgentOperation);
        messageTransportService.send(message, messageAsyncResult -> {
            if (messageAsyncResult.succeeded()) {
                ACLMessage responseMessage = messageAsyncResult.result();
                ActionStatus actionStatus = (ActionStatus) responseMessage.getObjectPayload();
                if (actionStatus.isSuccess()) {
                    handler.handle(Future.succeededFuture(ActionStatus.SUCCESS));
                } else {
                    handler.handle(Future.failedFuture("Impossible to send message killAgentOperation " + actionStatus.getCause()));
                }
            } else {
                handler.handle(Future.failedFuture("Impossible to send message killAgentOperation"));
            }
        });
    }
}
