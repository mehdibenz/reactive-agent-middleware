package com.indev.radex.ams.operation;

import com.indev.radex.ams.registry.AMSAgentRegistry;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public abstract class AMSOperation {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    public abstract void execute(Vertx vertx, AMSAgentRegistry registry, Handler<AsyncResult<Void>> handler);
}
