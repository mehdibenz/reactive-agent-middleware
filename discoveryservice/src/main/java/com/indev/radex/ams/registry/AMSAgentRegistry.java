package com.indev.radex.ams.registry;

import com.indev.radex.agent.AID;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by elmahdibenzekri on 05/11/2017.
 */
public class AMSAgentRegistry {
    private Map<AID, AMSAgentDescription> agentDirectoryEntries = new HashMap<>();

    public Optional<AMSAgentDescription> getDescription(AID aid) {
        return Optional.ofNullable(agentDirectoryEntries.get(aid));
    }

    public void register(AMSAgentDescription agentDescription) {
        agentDirectoryEntries.put(agentDescription.getName(), agentDescription);
    }

    public void deregister(AID aid) {
        agentDirectoryEntries.remove(aid);
    }

    public void modify(AMSAgentDescription agentDescription) {

    }

    public AID search(AMSAgentDescription agentDescription) {
        return null;
    }

    public Map<AID, AMSAgentDescription> getAgentDirectoryEntries() {
        return agentDirectoryEntries;
    }
}
