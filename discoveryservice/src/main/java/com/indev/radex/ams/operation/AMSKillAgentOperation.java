package com.indev.radex.ams.operation;

import com.indev.radex.agent.AID;
import com.indev.radex.ams.registry.AMSAgentDescription;
import com.indev.radex.ams.registry.AMSAgentRegistry;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;

import java.util.Optional;

public class AMSKillAgentOperation extends AMSOperation {
    private AID aid;

    public AMSKillAgentOperation(AID aid) {
        this.aid = aid;
    }

    @Override
    public void execute(Vertx vertx, AMSAgentRegistry registry, Handler<AsyncResult<Void>> handler) {
        Optional<AMSAgentDescription> agentDescription = registry.getDescription(aid);
        if (agentDescription.isPresent()) {
            vertx.undeploy(agentDescription.get().getDeploymentId(), voidAsyncResult -> {
                if (voidAsyncResult.succeeded()) {
                    registry.deregister(aid);
                    handler.handle(Future.succeededFuture());
                } else {
                    logger.error("Unexpected error while undeploying verticle " + aid);
                    handler.handle(Future.failedFuture(voidAsyncResult.cause()));
                }
            });
        } else {
            logger.error("Agent description not found when killing agent " + aid);
            handler.handle(Future.failedFuture("Agent description not found when killing agent " + aid));
        }
    }
}
