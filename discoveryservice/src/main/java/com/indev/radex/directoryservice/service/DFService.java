package com.indev.radex.directoryservice.service;

import com.indev.radex.directoryservice.operation.ActionStatus;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

/**
 * Created by elmahdibenzekri on 14/05/2017.
 */
@VertxGen
@ProxyGen
public interface DFService  {

    String ADDRESS = "radex.service.agentdirectory";

    void register(DFDescription dfDescription, Handler<AsyncResult<ActionStatus>> resultHandler);

    void modify(DFDescription dfDescription, Handler<AsyncResult<ActionStatus>> resultHandler);

    void deregister(DFDescription dfDescription, Handler<AsyncResult<ActionStatus>> resultHandler);

    void search(DFDescription dfDescription, Handler<AsyncResult<ActionStatus>> resultHandler);

}
