package com.indev.radex.directoryservice.operation;

import com.indev.radex.directoryservice.service.DFDescription;
import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

import java.util.Set;

/**
 * Created by elmahdibenzekri on 27/05/2017.
 */
@DataObject(generateConverter = true)
public class ActionStatus {
    public static ActionStatus SUCCESS = new ActionStatus(true);
    public static ActionStatus DUPLICATE = new ActionStatus(false, "DUPLICATE");
    public static ActionStatus INVALID = new ActionStatus(false, "INVALID");
    public static ActionStatus NOT_FOUND = new ActionStatus(false, "NOT_FOUND");
    public static ActionStatus ACCESS = new ActionStatus(false, "NOT_FOUND");

    private boolean success;
    private String code;
    private Set<DFDescription> entries;
    private String cause;

    public ActionStatus(boolean success) {
        this.success = success;
    }

    public ActionStatus(boolean success, String code) {
        this.success = success;
        this.code = code;
    }

    public ActionStatus() {
    }

    public ActionStatus(JsonObject jsonObject) {
        ActionStatusConverter.fromJson(jsonObject, this);
    }

    public JsonObject toJson() {
        JsonObject jsonObject = new JsonObject();
        ActionStatusConverter.toJson(this, jsonObject);
        return jsonObject;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ActionStatus success() {
        return this;
    }

    public ActionStatus faillure() {
        return this;
    }

    public Set<DFDescription> getEntries() {
        return entries;
    }

    public void setEntries(Set<DFDescription> entries) {
        this.entries = entries;
    }

    public String getCause() {
        return cause;
    }

    public ActionStatus withCause(String cause) {
        this.cause = cause;
        return this;
    }
}
