package com.indev.radex.directoryservice;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

import java.util.Set;

/**
 * Created by elmahdibenzekri on 10/12/2017.
 */
@DataObject(generateConverter = true)
public class ServiceDescription {
    private String name;
    private String type;
    private Set<String> protocols;
    private Set<String> ontologies;
    private Set<String> languages;
    private String ownership;
    private Set<String> properties;

    public ServiceDescription() {
    }

    public ServiceDescription(String name) {
        this.name = name;
    }

    public ServiceDescription(JsonObject jsonObject) {
        ServiceDescriptionConverter.fromJson(jsonObject, this);
    }

    public JsonObject toJson() {
        JsonObject jsonObject = new JsonObject();
        ServiceDescriptionConverter.toJson(this, jsonObject);
        return jsonObject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<String> getProtocols() {
        return protocols;
    }

    public void setProtocols(Set<String> protocols) {
        this.protocols = protocols;
    }

    public Set<String> getOntologies() {
        return ontologies;
    }

    public void setOntologies(Set<String> ontologies) {
        this.ontologies = ontologies;
    }

    public Set<String> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<String> languages) {
        this.languages = languages;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public Set<String> getProperties() {
        return properties;
    }

    public void setProperties(Set<String> properties) {
        this.properties = properties;
    }

    public ServiceDescription withProtocol(String protocol) {
        protocols.add(protocol);
        return this;
    }
}
