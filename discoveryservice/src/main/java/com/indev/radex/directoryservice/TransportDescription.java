package com.indev.radex.directoryservice;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

import java.util.Map;

/**
 * Created by elmahdibenzekri on 27/05/2017.
 */
@DataObject(generateConverter = true)
public class TransportDescription {
    private TransportType transportType;
    private String transportSpecificAddress;
    private Map<String, String> transportSpecificProperties;

    public TransportDescription(JsonObject fromJson) {
        TransportDescriptionConverter.fromJson(fromJson, this);
    }

    public JsonObject toJson() {
        JsonObject jsonObject = new JsonObject();
        TransportDescriptionConverter.toJson(this, jsonObject);
        return jsonObject;
    }

    public TransportDescription(TransportType transportType, String transportSpecificAddress, Map<String, String> transportSpecificProperties) {
        this.transportType = transportType;
        this.transportSpecificAddress = transportSpecificAddress;
        this.transportSpecificProperties = transportSpecificProperties;
    }

    public TransportType getTransportType() {
        return transportType;
    }

    public void setTransportType(TransportType transportType) {
        this.transportType = transportType;
    }

    public String getTransportSpecificAddress() {
        return transportSpecificAddress;
    }

    public void setTransportSpecificAddress(String transportSpecificAddress) {
        this.transportSpecificAddress = transportSpecificAddress;
    }

    public Map<String, String> getTransportSpecificProperties() {
        return transportSpecificProperties;
    }

    public void setTransportSpecificProperties(Map<String, String> transportSpecificProperties) {
        this.transportSpecificProperties = transportSpecificProperties;
    }
}
