package com.indev.radex.directoryservice.service;

import com.indev.radex.agent.AID;
import com.indev.radex.directoryservice.ServiceDescription;
import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by elmahdibenzekri on 10/12/2017.
 */
@DataObject(generateConverter = true)
public class DFDescription {
    private AID name;
    private Set<ServiceDescription> services = new HashSet<>();
    private Set<String> protocols;
    private Set<String> ontologies;
    private Set<String> languages;

    public DFDescription(AID name) {
        this.name = name;
    }

    public DFDescription() {
    }

    public DFDescription(JsonObject fromJson) {
        DFDescriptionConverter.fromJson(fromJson, this);
    }

    public JsonObject toJson() {
        JsonObject jsonObject = new JsonObject();
        DFDescriptionConverter.toJson(this, jsonObject);
        return jsonObject;
    }

    public void addService(ServiceDescription serviceDescription) {
        services.add(serviceDescription);
    }

    public AID getName() {
        return name;
    }

    public void setName(AID name) {
        this.name = name;
    }

    public Set<ServiceDescription> getServices() {
        return services;
    }

    public void setServices(Set<ServiceDescription> services) {
        this.services = services;
    }

    public Set<String> getProtocols() {
        return protocols;
    }

    public void setProtocols(Set<String> protocols) {
        this.protocols = protocols;
    }

    public Set<String> getOntologies() {
        return ontologies;
    }

    public void setOntologies(Set<String> ontologies) {
        this.ontologies = ontologies;
    }

    public Set<String> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<String> languages) {
        this.languages = languages;
    }

    public DFDescription withService(ServiceDescription serviceDescription) {
        services.add(serviceDescription);
        return this;
    }
}
