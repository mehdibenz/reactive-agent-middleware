package com.indev.radex.directoryservice.operation;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class ActionStatusCodec implements MessageCodec<ActionStatus, ActionStatus> {

    @Override
    public void encodeToWire(Buffer buffer, ActionStatus actionStatus) {
        final JsonObject jsonToEncode = new JsonObject();
        jsonToEncode.put("code", actionStatus.getCode());
        JsonArray entries = new JsonArray();
        actionStatus.getEntries().forEach(entry -> {
            entries.add(entry.toJson());
        });
        jsonToEncode.put("entries", entries);

        // Encode object to string
        final String jsonToStr = jsonToEncode.encode();

        // Length of JSON: is NOT characters count
        final int length = jsonToStr.getBytes().length;

        // Write data into given buffer
        buffer.appendInt(length);
        buffer.appendString(jsonToStr);
    }

    @Override
    public ActionStatus decodeFromWire(int i, Buffer buffer) {
        // My custom message starting from this *position* of buffer
        int _pos = i;

        // Length of JSON
        final int length = buffer.getInt(_pos);

        // Get JSON string by it`s length
        // Jump 4 because getInt() == 4 bytes
        final String jsonStr = buffer.getString(_pos += 4, _pos + length);
        final JsonObject contentJson = new JsonObject(jsonStr);

        return new ActionStatus(contentJson);
    }

    @Override
    public ActionStatus transform(ActionStatus actionStatus) {
        return actionStatus;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }

}
