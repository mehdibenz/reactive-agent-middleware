package com.indev.radex.directoryservice.service;

import com.indev.radex.agent.AID;
import com.indev.radex.directoryservice.operation.ActionStatus;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.servicediscovery.ServiceDiscovery;

import java.util.*;

/**
 * Created by elmahdibenzekri on 27/05/2017.
 */
public class DefaultDFService implements DFService {
    private Map<AID, DFDescription> agentDirectoryEntries = new HashMap<>();
    private DFDescriptionValidator dfDescriptionValidator = new DFDescriptionValidator();
    private AgentDirectoryMatcher agentDirectoryMatcher= new AgentDirectoryMatcher();
    public DefaultDFService(Vertx vertx, ServiceDiscovery serviceDiscovery) {

    }

    @Override
    public void register(DFDescription dfDescription, Handler<AsyncResult<ActionStatus>> resultHandler) {
        if (!dfDescriptionValidator.validate(dfDescription)) {
            resultHandler.handle(Future.succeededFuture(ActionStatus.INVALID));
            return;
        }
        if (agentDirectoryEntries.containsKey(dfDescription.getName())) {
            resultHandler.handle(Future.succeededFuture(ActionStatus.DUPLICATE));
            return;
        }
        agentDirectoryEntries.put(dfDescription.getName(), dfDescription);
        resultHandler.handle(Future.succeededFuture(ActionStatus.SUCCESS));
    }

    public void modify(DFDescription dfDescription, Handler<AsyncResult<ActionStatus>> resultHandler) {
        if (!dfDescriptionValidator.validate(dfDescription)) {
            resultHandler.handle(Future.succeededFuture(ActionStatus.INVALID));
            return;
        }
        if (!agentDirectoryEntries.containsKey(dfDescription.getName())) {
            resultHandler.handle(Future.succeededFuture(ActionStatus.NOT_FOUND));
            return;
        }
        // TODO replace kvts
        agentDirectoryEntries.put(dfDescription.getName(), dfDescription);
        resultHandler.handle(Future.succeededFuture(ActionStatus.SUCCESS));
    }

    public void deregister(DFDescription dfDescription, Handler<AsyncResult<ActionStatus>> resultHandler) {
        if (!dfDescriptionValidator.validate(dfDescription)) {
            resultHandler.handle(Future.succeededFuture(ActionStatus.INVALID));
            return;
        }
        if (!agentDirectoryEntries.containsKey(dfDescription.getName())) {
            resultHandler.handle(Future.succeededFuture(ActionStatus.NOT_FOUND));
            return;
        }
        agentDirectoryEntries.remove(dfDescription.getName());
        resultHandler.handle(Future.succeededFuture(ActionStatus.SUCCESS));
    }

    public void search(DFDescription dfDescription, Handler<AsyncResult<ActionStatus>> resultHandler) {
        if (!dfDescriptionValidator.validate(dfDescription)) {
            resultHandler.handle(Future.succeededFuture(ActionStatus.INVALID));
            return;
        }
        Set<DFDescription> matches = new HashSet<>();
        Iterator<DFDescription> iterator = agentDirectoryEntries.values().iterator();
        while (iterator.hasNext()) {
            DFDescription next = iterator.next();
            if (agentDirectoryMatcher.matches(dfDescription, next)) {
                matches.add(next);
            }
        }
        if (matches.isEmpty()) {
            resultHandler.handle(Future.succeededFuture(ActionStatus.NOT_FOUND));
        }
        ActionStatus success = ActionStatus.SUCCESS;
        success.setEntries(matches);
        resultHandler.handle(Future.succeededFuture(success));
    }
}
