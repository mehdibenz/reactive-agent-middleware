package com.indev.radex.directoryservice.service;

import com.indev.radex.agent.AID;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AgentDirectoryMatcher {


    public boolean matches(DFDescription templateDF, DFDescription otherDF) {
        JsonObject templateDFJson = templateDF.toJson();
        JsonObject otherDFJson = otherDF.toJson();

        try {
            jsonCheck(templateDFJson, otherDFJson);
        } catch (DFNotMatching ex) {
            return false;
        }

        return true;
    }

    private boolean jsonCheck(JsonObject templateDFJson, JsonObject otherDFJson) {
        for (Map.Entry<String, Object> objectEntry : templateDFJson) {
            if (objectEntry.getValue() instanceof String) {
                if (!objectEntry.getValue().equals(otherDFJson.getString(objectEntry.getKey()))) {
                    throw new DFNotMatching();
                }
            } else if (objectEntry.getValue() instanceof JsonObject) {
                jsonCheck((JsonObject) objectEntry.getValue(), otherDFJson.getJsonObject(objectEntry.getKey()));
            } else if (objectEntry.getValue() instanceof JsonArray) {
                JsonArray array = (JsonArray) objectEntry.getValue();
                JsonArray otherArray = otherDFJson.getJsonArray(objectEntry.getKey());
                for (int i = 0; i < array.size(); i++) {
                    for (int j = 0; j < otherArray.size(); j++) {
                        try {
                            if (array.getValue(i) instanceof String) {
                                if (stringCheck(array.getString(i), otherArray.getString(i))) {
                                    return true;
                                }
                            } else if (array.getValue(i) instanceof JsonObject) {
                                if (jsonCheck(array.getJsonObject(i), otherArray.getJsonObject(i))) {
                                    return true;
                                }
                            }
                        } catch (DFNotMatching ex) {
                        }
                    }
                    throw new DFNotMatching();
                }
            }
        }
        return true;
    }

    public boolean stringCheck(String template, String other) {
        if (!template.equals(other)) {
            throw new DFNotMatching();
        }
        return true;
    }

    private AID propertyMatchers(DFDescription dfDescription) {
        List<PropertyMatcher> templateDF = new ArrayList<>();
        AID aid = dfDescription.getName();
        templateDF.add(new StringPropertyMatcher(aid.getName()));
        templateDF.add(new SetPropertyMatcher(aid.getResolvers()));
        aid.getAddresses().forEach(address -> {
            templateDF.add(new StringPropertyMatcher(address.getTransportSpecificAddress()));
            templateDF.add(new StringPropertyMatcher(address.getTransportType().name()));//todo enum
            //templateDF.add(new StringPropertyMatcher(address.getTransportSpecificProperties()));

        });
        return aid;
    }
}

abstract class PropertyMatcher<T> {
    T property;

    public PropertyMatcher(T property) {
        this.property = property;
    }

    abstract boolean matches(T other);
}

class StringPropertyMatcher extends PropertyMatcher<String> {

    public StringPropertyMatcher(String property) {
        super(property);
    }

    @Override
    boolean matches(String other) {
        if (property.isEmpty()) {
            return true;
        }
        return property.equals(other);
    }
}

class SetPropertyMatcher extends PropertyMatcher<Set<String>> {

    public SetPropertyMatcher(Set<String> property) {
        super(property);
    }

    @Override
    boolean matches(Set<String> other) {
        if (property.isEmpty()) {
            return true;
        }
        if (property.size() != other.size()) {
            return false;
        }
        return property.equals(other);
    }
}
