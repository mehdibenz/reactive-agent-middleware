package com.indev.radex.directoryservice;

/**
 * Created by elmahdibenzekri on 27/05/2017.
 */
public enum TransportType {
    HTTP, EVENT
}
