package com.indev.radex.communication.sl;

import java.util.HashMap;
import java.util.Map;

public class SLBuilder {

    private String action;
    private Map<String, String> params = new HashMap<>();

    public SLBuilder action(String action) {
        this.action = action;
        return this;
    }

    public SLBuilder param(String name, String value) {
        params.put(name, value);
        return this;
    }

    public String build() {
        StringBuilder result = new StringBuilder("(");
        result.append(action);
        for (String paramKey : params.keySet()) {
            result.append(" :");
            result.append(paramKey);
            result.append(" ");
            result.append(params.get(paramKey));
        }
        return result.toString();
    }
}
