package com.indev.radex.agent.behavior;

import com.indev.radex.agent.communication.AgentCommunication;
import com.indev.radex.messageservice.Performative;

public class BehaviorConfigurationFactory {
    private AgentCommunication communication;
    private BehaviorExecutor agentBehavior;

    public BehaviorConfigurationFactory(AgentCommunication communication, BehaviorExecutor agentBehavior) {
        this.communication = communication;
        this.agentBehavior = agentBehavior;
    }

    public BehaviorConfiguration onMessage(Performative performative) {
        return new OnMessageReceivedBehaviorConfig(communication, performative);
    }

    public BehaviorConfiguration cyclic(long interval) {
        return new CyclicBehaviorConfig(interval);
    }

    public BehaviorConfiguration sequential() {
        return new SequentialBehaviorConfig(agentBehavior);
    }
}
