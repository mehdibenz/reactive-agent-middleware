package com.indev.radex.agent.test;

import com.indev.radex.agent.lifecycle.AbstractAgent;
import com.indev.radex.directoryservice.ServiceDescription;
import com.indev.radex.directoryservice.service.DFDescription;
import com.indev.radex.messageservice.Performative;
import com.indev.radex.messageservice.message.ACLMessage;
import io.vertx.core.Future;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by elmahdibenzekri on 14/05/2017.
 */
public class SellerAgent extends AbstractAgent {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private Map<String, Integer> booksWithPrices = new HashMap<>();

    @Override
    public void initialize(Future initialisationFuture) {
        booksWithPrices.put("Java 8", 124);
        booksWithPrices.put("Swift", 38);

        DFDescription dfDescription = new DFDescription(getAgentId());
        ServiceDescription service = new ServiceDescription();
        service.setType("book-selling");
        service.setName("test-book-trading");
        dfDescription.addService(service);
        dfService.register(dfDescription, actionStatusAsyncResult -> {
            addBehavior(behaviorContext -> {
                ACLMessage aclMessage = behaviorContext.lastMessage();
                String bookTitle = aclMessage.getPayload();
                logger.info("Received request for book " + bookTitle);
                if (booksWithPrices.containsKey(bookTitle)) {
                    ACLMessage reply = aclMessage.createReply();
                    reply.setPayload("" + booksWithPrices.get(bookTitle));
                    communication.reply(behaviorContext, reply);
                    logger.info("Response sent with price " + booksWithPrices.get(bookTitle));
                }
            }, behaviorConfigurationFactory.onMessage(Performative.CFP));
            initialisationFuture.complete();
        });

        /*communication.onReceive(aclMessage -> Performative.CFP.equals(aclMessage.getPerformative()), aclMessage -> {
            String bookTitle = aclMessage.getPayload();
            logger.info("Received request for book " + bookTitle);
            if (booksWithPrices.containsKey(bookTitle)) {
                ACLMessage reply = aclMessage.createReply();
                reply.setPayload("" + booksWithPrices.get(bookTitle));
                communication.publish(reply);
                logger.info("Response sent with price " + booksWithPrices.get(bookTitle));
            }
        });*/
    }

}

