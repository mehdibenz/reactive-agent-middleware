package com.indev.radex.agent.test;

import com.indev.radex.agent.AID;
import com.indev.radex.agent.behavior.Behavior;
import com.indev.radex.agent.lifecycle.AbstractAgent;
import com.indev.radex.directoryservice.ServiceDescription;
import com.indev.radex.directoryservice.service.DFDescription;
import com.indev.radex.messageservice.Performative;
import com.indev.radex.messageservice.message.ACLMessage;
import io.vertx.core.Future;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.Optional;

/**
 * Created by elmahdibenzekri on 14/05/2017.
 */
public class BuyerAgent extends AbstractAgent {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private Optional<AID> bookSellerAID = Optional.empty();

    @Override
    public void initialize(Future initialisationFuture) {
        setupBehaviors();
        initialisationFuture.complete();
    }

    private void setupBehaviors() {
        addBehavior(requestPriceBehavior(), behaviorConfigurationFactory.cyclic(10000));
        Behavior eatBehavior = context1 -> {
            logger.info("eat behaviorExecutor");
        };
        Behavior sleepBehavior = context1 -> {
            logger.info("sleep behaviorExecutor");
        };
        Behavior repeatBehavior = context1 -> {
            logger.info("repeat behaviorExecutor");
        };
        addBehavior(eatBehavior, behaviorConfigurationFactory.sequential());
        addBehavior(new SequentialBehavior().add(eatBehavior).add(sleepBehavior).add(repeatBehavior), behaviorConfigurationFactory.sequential());//todo think about making sequential behaviorExecutor or implementing the logic in AgentBehavior
    }

    private Behavior requestPriceBehavior() {
        return behaviorContext -> {
            if (bookSellerAID.isPresent()) {
                logger.info("Book seller is known " + bookSellerAID.get());
                buildRequests();
            } else {
                DFDescription dfDescription = sellerDFTemplate();
                logger.info("Book seller is unknown, df search "+ dfDescription);
                dfService.search(dfDescription, actionStatusAsyncResult -> {
                    if (actionStatusAsyncResult.succeeded() && actionStatusAsyncResult.result().isSuccess()) {
                        bookSellerAID = Optional.ofNullable(actionStatusAsyncResult.result().getEntries().iterator().next().getName());
                        logger.warn("Book seller found within df " + dfDescription);
                        buildRequests();
                    } else {
                        logger.warn("Book seller not found with df " + dfDescription);
                    }
                });
            }
        };
    }

    private DFDescription sellerDFTemplate() {
        DFDescription dfDescription = new DFDescription();
        ServiceDescription service = new ServiceDescription();
        service.setType("book-selling");
        dfDescription.addService(service);
        return dfDescription;
    }

    private void buildRequests() {
        ACLMessage message = requestPriceFor("Java 8");

        communication.onSuccessfulResponseReceived(message, aclMessage -> {
            if (Performative.PROPOSE.equals(aclMessage.getPerformative())) {
                Integer.parseInt(aclMessage.getPayload());
            }
            logger.info("Received response " + aclMessage.getPayload());
        });
        ACLMessage message2 = requestPriceFor("Swift");

        communication.onSuccessfulResponseReceived(message2, aclMessage -> {
            if (Performative.PROPOSE.equals(aclMessage.getPerformative())) {
                Integer.parseInt(aclMessage.getPayload());
            }
            logger.info("Received response " + aclMessage.getPayload());
        });
    }

    private ACLMessage requestPriceFor(String bookTitle) {
        ACLMessage message = new ACLMessage();
        message.setPerformative(Performative.CFP);
        message.setPayload(bookTitle);
        message.setConversationId("book-trade");
        message.setReplyWith("conversation1");
        message.addReceiver(bookSellerAID.get());
        return message;
    }
}
