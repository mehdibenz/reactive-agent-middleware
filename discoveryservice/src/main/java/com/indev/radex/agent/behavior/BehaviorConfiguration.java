package com.indev.radex.agent.behavior;

import com.indev.radex.agent.communication.AgentCommunication;
import com.indev.radex.messageservice.Performative;
import io.vertx.core.Vertx;

public abstract class BehaviorConfiguration {
    public abstract void configure(Vertx vertx, Behavior behavior);
}

class OnMessageReceivedBehaviorConfig extends BehaviorConfiguration {
    private AgentCommunication communication;
    private Performative performative;

    public OnMessageReceivedBehaviorConfig(AgentCommunication communication, Performative performative) {
        this.communication = communication;
        this.performative = performative;
    }

    @Override
    public void configure(Vertx vertx, Behavior behavior) {
        communication.onACLMessageReceived(aclMessage -> performative.equals(aclMessage.getPerformative()), aclMessage -> {
            behavior.action(new BehaviorContext().withMessage(aclMessage));
        });
    }
}

class CyclicBehaviorConfig extends BehaviorConfiguration {

    private long interval;

    public CyclicBehaviorConfig(long interval) {
        this.interval = interval;
    }

    @Override
    public void configure(Vertx vertx, Behavior behavior) {
        vertx.setPeriodic(interval, aLong -> behavior.action(new BehaviorContext()));
    }
}

class SequentialBehaviorConfig extends BehaviorConfiguration {

    private BehaviorExecutor agentBehavior;

    public SequentialBehaviorConfig(BehaviorExecutor agentBehavior) {
        this.agentBehavior = agentBehavior;
    }

    @Override
    public void configure(Vertx vertx, Behavior behavior) {
        agentBehavior.pushToExecutionQueue(behavior);
    }
}
