package com.indev.radex.agent.behavior;

import com.indev.radex.messageservice.message.ACLMessage;
import io.vertx.core.eventbus.Message;

public class BehaviorContext {

    private Message<ACLMessage> message;

    public BehaviorContext() {
        this.message = message;
    }

    public BehaviorContext withMessage(Message<ACLMessage> message){
        this.message=message;
        return this;
    }

    public ACLMessage lastMessage() {
        return message.body();
    }

    public Message vertxMessage(){
        return message;
    }
}