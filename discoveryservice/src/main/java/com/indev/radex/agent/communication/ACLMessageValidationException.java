package com.indev.radex.agent.communication;

public class ACLMessageValidationException extends Exception {
    public ACLMessageValidationException(String message) {
        super(message);
    }
}
