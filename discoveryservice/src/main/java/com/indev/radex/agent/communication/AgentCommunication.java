package com.indev.radex.agent.communication;

import com.indev.radex.agent.AID;
import com.indev.radex.agent.behavior.BehaviorContext;
import com.indev.radex.ams.service.AMSService;
import com.indev.radex.directoryservice.service.DFService;
import com.indev.radex.messageservice.MessageTransportService;
import com.indev.radex.messageservice.message.ACLMessage;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Predicate;

/**
 * Created by elmahdibenzekri on 04/11/2017.
 */
public class AgentCommunication {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private MessageTransportService messageTransportService;
    private AgentCommunicationListener communicationListener;
    private DFService dfService;
    private AMSService amsService;
    private Vertx vertx;
    private AID aid;
    private Map<Predicate<ACLMessage>, Handler<Message<ACLMessage>>> handlers = new HashMap<>();

    public AgentCommunication(MessageTransportService messageTransportService, DFService dfService, AMSService amsService, Vertx vertx, AID aid) {
        this.messageTransportService = messageTransportService;
        this.dfService = dfService;
        this.amsService = amsService;
        this.vertx = vertx;
        this.aid = aid;
        this.communicationListener = new AgentCommunicationListener(aid);
    }

    public void init() {
        vertx.eventBus().<ACLMessage>consumer("agent." + aid.getName(), message -> {
            logger.info("Received message " + aid.getName() + " : " + message);
            ACLMessage aclMessage = message.body();
            Iterator<Map.Entry<Predicate<ACLMessage>, Handler<Message<ACLMessage>>>> iterator = handlers.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<Predicate<ACLMessage>, Handler<Message<ACLMessage>>> next = iterator.next();
                if (next.getKey().test(aclMessage)) {
                    next.getValue().handle(message);
                }
            }
        });

        vertx.eventBus().consumer("agent." + aid.getName() + ".destroy", message -> {
            logger.info("Received request to destroy agent " + aid.getName());
        });
    }

    public void onMessageReceived(Handler<Message<ACLMessage>> handler) {
        handlers.put(aclMessage -> true, handler);
    }

    public void onMessageReceived(Predicate<ACLMessage> predicate, Handler<Message<ACLMessage>> handler) {
        handlers.put(predicate, handler);
    }

    public void onACLMessageReceived(Handler<ACLMessage> handler) {
        handlers.put(message -> true, message -> handler.handle(message.body()));
    }

    public void onACLMessageReceived(Predicate<ACLMessage> predicate, Handler<Message<ACLMessage>> handler) {
        handlers.put(predicate, message -> handler.handle(message));
    }

    public boolean send(ACLMessage message) {
        try {
            communicationListener.beforeSending(message);
            messageTransportService.publish(message);
            return true;
        } catch (ACLMessageValidationException ex) {
            return false;
        }
    }

    public void onSuccessfulResponseReceived(ACLMessage message, Handler<ACLMessage> handler) {
        onResponseReceived(message, aclMessageAsyncResult -> {
            if (aclMessageAsyncResult.succeeded()) {
                handler.handle(aclMessageAsyncResult.result());
            } else {
                logger.warn("Error while waiting for response of " + message);
            }
        });
    }

    public void onResponseReceived(ACLMessage message, Handler<AsyncResult<ACLMessage>> handler) {
        try {
            communicationListener.beforeSending(message);
        } catch (ACLMessageValidationException ex) {
            handler.handle(Future.failedFuture(ex));
        }

        Predicate<ACLMessage> predicate = m -> message.getConversationId().equals(m.getConversationId()) && message.getReplyWith().equals(m.getReplyWith());
        messageTransportService.send(message, aclMessageAsyncResult -> {
            if (aclMessageAsyncResult.succeeded()) {
                if (aclMessageAsyncResult.succeeded() && predicate.test(aclMessageAsyncResult.result())) {
                    handler.handle(Future.succeededFuture(aclMessageAsyncResult.result()));
                } else {
                    handler.handle(Future.failedFuture("Missing conversation id from ACLMessage"));
                }
            } else {
                handler.handle(Future.failedFuture(aclMessageAsyncResult.cause()));
            }
        });
        //onACLMessageReceived(predicate, handler);
    }

    public DFService getDfService() {
        return dfService;
    }

    public AMSService getAmsService() {
        return amsService;
    }

    public void stop() {
        handlers.clear();
    }

    public boolean reply(BehaviorContext behaviorContext, ACLMessage reply) {
        try {
            communicationListener.beforeSending(reply);
            behaviorContext.vertxMessage().reply(reply);
            logger.info("Sending reply success " + reply);
            return true;
        } catch (ACLMessageValidationException ex) {
            logger.info("Sending reply error " + reply);
            return false;
        }
    }
}
