package com.indev.radex.agent.communication;

import com.indev.radex.messageservice.message.ACLMessage;

public class AgentCommunicationValidator {
    public void validate(ACLMessage message) throws ACLMessageValidationException {
        if (message.getSender() == null || message.getSender().getName() == null) {
            throw new ACLMessageValidationException("Missing sender from ACLMessage");
        }
        if (message.getReceivers().isEmpty()) {
            throw new ACLMessageValidationException("Missing receivers from ACLMessage");
        }
    }
}
