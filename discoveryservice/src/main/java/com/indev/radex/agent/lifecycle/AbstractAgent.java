package com.indev.radex.agent.lifecycle;

import com.indev.radex.agent.AID;
import com.indev.radex.agent.behavior.Behavior;
import com.indev.radex.agent.behavior.BehaviorConfiguration;
import com.indev.radex.agent.behavior.BehaviorConfigurationFactory;
import com.indev.radex.agent.behavior.BehaviorExecutor;
import com.indev.radex.agent.communication.AgentCommunication;
import com.indev.radex.ams.service.AMSService;
import com.indev.radex.directoryservice.service.DFService;
import com.indev.radex.messageservice.MessageTransportService;
import io.vertx.core.*;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import io.vertx.servicediscovery.types.EventBusService;

/**
 * Created by elmahdibenzekri on 09/09/2017.
 */
public abstract class AbstractAgent extends AbstractVerticle {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    protected AID aid;
    protected BehaviorExecutor behaviorExecutor;
    protected AgentCommunication communication;
    protected DFService dfService;
    protected BehaviorConfigurationFactory behaviorConfigurationFactory;
    protected ServiceDiscovery serviceDiscovery;
    private AgentConfigurator agentConfigurator = new AgentConfigurator(this);

    @Override
    public void start(Future startFuture) throws Exception {
        logger.info("Starting agent " + this);
        setupServices(asyncResult -> {
            if (asyncResult.succeeded()) {

                initialisation(startFuture);
            } else {
                startFuture.fail("Impossible to start agent " + aid);
            }
        });
    }

    private void initialisation(Future initialisationFuture) {
        behaviorExecutor = new BehaviorExecutor();
        behaviorConfigurationFactory = new BehaviorConfigurationFactory(communication, behaviorExecutor);

        initialize(initialisationFuture);//todo call initialize when all proxy services are retrieved

        behaviorExecutor.start();//todo start behaviors when init is done
    }

    private void setupServices(Handler<AsyncResult> handler) {
        serviceDiscovery = ServiceDiscovery.create(vertx, new ServiceDiscoveryOptions().setBackendConfiguration(config()));

        Future<MessageTransportService> messageTransportServiceFuture = Future.future();
        EventBusService.getProxy(serviceDiscovery, MessageTransportService.class, messageTransportServiceFuture);

        Future<AMSService> amsService = Future.future();
        EventBusService.getProxy(serviceDiscovery, AMSService.class, amsService);

        Future<DFService> agentDirectoryFuture = Future.future();
        EventBusService.getProxy(serviceDiscovery, DFService.class, agentDirectoryFuture);

        CompositeFuture.all(messageTransportServiceFuture, agentDirectoryFuture, amsService).setHandler(compositeFutureAsyncResult -> {
            if (compositeFutureAsyncResult.failed()) {
                logger.error("failed to retrieve agent services");
                handler.handle(Future.failedFuture(compositeFutureAsyncResult.cause()));
            } else {
                dfService = agentDirectoryFuture.result();
                agentConfigurator.configure(config());
                communication = new AgentCommunication(messageTransportServiceFuture.result(), dfService, amsService.result(), vertx, getAgentId());
                communication.init();
                handler.handle(Future.succeededFuture());
            }
        });
    }


    @Override
    public void stop() throws Exception {
        //todo unregister from ams and df
        communication.stop();
        behaviorExecutor.stop();
    }


    protected AID getAgentId() {
        return aid;
    }

    protected abstract void initialize(Future initialisationFuture);

    protected void addBehavior(Behavior behavior, BehaviorConfiguration behaviorConfiguration) {
        behaviorConfiguration.configure(vertx, behavior);
    }
}
