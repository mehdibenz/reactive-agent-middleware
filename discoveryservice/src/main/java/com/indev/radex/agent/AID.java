package com.indev.radex.agent;

import com.indev.radex.directoryservice.TransportDescription;
import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by elmahdibenzekri on 02/07/2017.
 */
@DataObject(generateConverter = true)
public class AID {
    private String name;
    private List<TransportDescription> addresses = new ArrayList<>();
    private Set<String> resolvers = new HashSet<>();

    public AID() {
    }

    public AID(JsonObject jsonObject) {
        AIDConverter.fromJson(jsonObject, this);
    }

    public JsonObject toJson() {
        JsonObject jsonObject = new JsonObject();
        AIDConverter.toJson(this, jsonObject);
        return jsonObject;
    }

    public AID(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TransportDescription> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<TransportDescription> addresses) {
        this.addresses = addresses;
    }

    public Set<String> getResolvers() {
        return resolvers;
    }

    public void setResolvers(Set<String> resolvers) {
        this.resolvers = resolvers;
    }

    public AID with(TransportDescription transportDescription) {
        this.addresses.add(transportDescription);
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        String otherName = ((AID) obj).getName();
        return super.equals(obj) || name.equals(otherName);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "AID{" +
                "name='" + name + '\'' +
                ", addresses=" + addresses +
                ", resolvers=" + resolvers +
                '}';
    }
}
