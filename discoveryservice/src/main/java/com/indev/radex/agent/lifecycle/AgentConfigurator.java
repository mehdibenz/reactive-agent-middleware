package com.indev.radex.agent.lifecycle;

import com.indev.radex.agent.AID;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class AgentConfigurator {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private AbstractAgent agent;

    public AgentConfigurator(AbstractAgent agent) {
        this.agent = agent;
    }

    public void configure(JsonObject config) {
        String aid = config.getString("AID");
        if (aid == null) {
            throw new IllegalStateException("No AID set for agent " + agent);
        }
        agent.aid = new AID(aid);//extract AID creation
        logger.info("Agent " + agent.getClass() + " started with AID = " + agent.aid);
    }
}
