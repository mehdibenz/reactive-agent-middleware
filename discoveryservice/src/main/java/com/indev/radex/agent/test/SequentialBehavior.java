package com.indev.radex.agent.test;

import com.indev.radex.agent.behavior.Behavior;
import com.indev.radex.agent.behavior.BehaviorContext;

import java.util.ArrayList;
import java.util.List;

public class SequentialBehavior implements Behavior {
    List<Behavior> behaviors = new ArrayList<>();
    @Override
    public void action(BehaviorContext context) {
        behaviors.forEach(behavior -> behavior.action(context));
    }

    public SequentialBehavior add(Behavior behavior) {
        behaviors.add(behavior);
        return this;
    }
}
