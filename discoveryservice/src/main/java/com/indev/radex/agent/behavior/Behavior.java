package com.indev.radex.agent.behavior;

/**
 * Created by elmahdibenzekri on 11/11/2017.
 */
@FunctionalInterface
public interface Behavior {
    void action(BehaviorContext context);
}