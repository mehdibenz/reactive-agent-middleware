package com.indev.radex.agent;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class AIDCodec implements MessageCodec<AID, AID> {

    @Override
    public void encodeToWire(Buffer buffer, AID aid) {
        final JsonObject jsonToEncode = new JsonObject();
        jsonToEncode.put("name", aid.getName());
        JsonArray entries = new JsonArray();
        /*actionStatus.getEntries().forEach(entry -> {
            entries.add(entry.toJson());
        });*///todo implement codec for aid
        jsonToEncode.put("entries", entries);

        // Encode object to string
        final String jsonToStr = jsonToEncode.encode();

        // Length of JSON: is NOT characters count
        final int length = jsonToStr.getBytes().length;

        // Write data into given buffer
        buffer.appendInt(length);
        buffer.appendString(jsonToStr);
    }

    @Override
    public AID decodeFromWire(int i, Buffer buffer) {
        // My custom message starting from this *position* of buffer
        int _pos = i;

        // Length of JSON
        final int length = buffer.getInt(_pos);

        // Get JSON string by it`s length
        // Jump 4 because getInt() == 4 bytes
        final String jsonStr = buffer.getString(_pos += 4, _pos + length);
        final JsonObject contentJson = new JsonObject(jsonStr);

        return new AID(contentJson);
    }

    @Override
    public AID transform(AID aid) {
        return aid;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }

}
