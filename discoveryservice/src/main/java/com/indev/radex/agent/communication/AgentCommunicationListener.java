package com.indev.radex.agent.communication;

import com.indev.radex.agent.AID;
import com.indev.radex.messageservice.message.ACLMessage;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class AgentCommunicationListener {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private AgentCommunicationValidator communicationValidator = new AgentCommunicationValidator();
    private AID aid;

    public AgentCommunicationListener(AID aid) {
        this.aid = aid;
    }

    public void beforeSending(ACLMessage message) throws ACLMessageValidationException {
        if (message.getSender() == null) {
            message.setSender(aid);
        }
        try {
            communicationValidator.validate(message);
        } catch (RuntimeException ex) {
            logger.warn(ex.getMessage());
            throw ex;
        }
    }
}
