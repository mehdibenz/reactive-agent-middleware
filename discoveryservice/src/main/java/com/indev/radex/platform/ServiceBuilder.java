package com.indev.radex.platform;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.EventBusService;
import io.vertx.serviceproxy.ServiceBinder;
import io.vertx.serviceproxy.ServiceProxyBuilder;

public class ServiceBuilder {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Vertx vertx;
    private final ServiceDiscovery serviceDiscovery;

    public ServiceBuilder(Vertx vertx, ServiceDiscovery serviceDiscovery) {
        this.vertx = vertx;
        this.serviceDiscovery = serviceDiscovery;
    }

    public <T> void createService(String serviceName, String serviceAddress, T service, Class<T> serviceClass) {
        createService(serviceName, serviceAddress, service, serviceClass, asyncResult -> {
            //todo empty handler
        });
    }

    public <T> void createService(String serviceName, String serviceAddress, T service, Class<T> serviceClass, Handler<AsyncResult> handler) {
        logger.info("Registering service " + serviceName + " class=" + serviceClass + " service=" + service + " address=" + serviceAddress);

        new ServiceBinder(vertx)
                .setAddress(serviceAddress)
                .register(serviceClass, service);

        publishEventBusService(serviceName, serviceAddress, serviceClass, ar -> {
            if (ar.failed()) {
                ar.cause().printStackTrace();
                handler.handle(Future.failedFuture(ar.cause()));
            } else {
                logger.info(serviceName + " published : " + ar.succeeded());
                handler.handle(Future.succeededFuture());
            }
        });
    }

    public <T> T getServiceProxy(String serviceAddress, Class<T> serviceClass){
        ServiceProxyBuilder builder = new ServiceProxyBuilder(vertx).setAddress(serviceAddress);
        return builder.build(serviceClass);
    }

    private void publishEventBusService(String name, String address, Class serviceClass, Handler<AsyncResult<Void>>
            completionHandler) {
        Record record = EventBusService.createRecord(name, address, serviceClass);
        publish(record, completionHandler);
    }

    protected void publish(Record record, Handler<AsyncResult<Void>> completionHandler) {
        serviceDiscovery.publish(record, ar -> {
            if (ar.succeeded()) {
                //registeredRecords.add(record);
                completionHandler.handle(ar.map((Void) null));
            } else {
                completionHandler.handle(Future.failedFuture(ar.cause()));
            }
        });
    }
}
