package com.indev.radex.platform;

import com.indev.radex.agent.AID;
import com.indev.radex.ams.AMS;
import com.indev.radex.ams.service.AMSService;
import com.indev.radex.ams.service.DefaultAMSService;
import com.indev.radex.directoryservice.service.DFService;
import com.indev.radex.directoryservice.service.DefaultDFService;
import com.indev.radex.messageservice.DefaultMessageTransportService;
import com.indev.radex.messageservice.MessageTransportService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;

/**
 * Created by elmahdibenzekri on 14/05/2017.
 */
public class APVerticle extends AbstractVerticle {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private ServiceDiscovery serviceDiscovery;
    private ServiceBuilder serviceBuilder;
    private AID amsAID;

    @Override
    public void start(Future<Void> future) throws Exception {
        initPlatform(future);
    }

    private void initPlatform(Future<Void> future) {
        serviceDiscovery = ServiceDiscovery.create(vertx, new ServiceDiscoveryOptions().setBackendConfiguration(config()));
        serviceBuilder = new ServiceBuilder(vertx, serviceDiscovery);

        DefaultMessageTransportService messageTransportService = new DefaultMessageTransportService(vertx);
        DefaultDFService dfService = new DefaultDFService(vertx, serviceDiscovery);
        amsAID = new AID("ams");
        DefaultAMSService amsService = new DefaultAMSService(messageTransportService, amsAID);

        Future messageTransportServiceFuture = Future.future();
        Future dfServiceFuture = Future.future();
        Future amsServiceFuture = Future.future();

        serviceBuilder.createService("agentDiscovery", DFService.ADDRESS, dfService, DFService.class, dfServiceFuture.completer());
        serviceBuilder.createService("messageTransport", MessageTransportService.ADDRESS, messageTransportService, MessageTransportService.class, messageTransportServiceFuture.completer());
        serviceBuilder.createService("amsService", AMSService.ADDRESS, amsService, AMSService.class, amsServiceFuture.completer());

        CompositeFuture.all(messageTransportServiceFuture, dfServiceFuture, amsServiceFuture).setHandler(compositeFutureAsyncResult -> {
            messageTransportServiceFuture.result();
            if (compositeFutureAsyncResult.succeeded()) {
                logger.info("AP started");
                vertx.deployVerticle(new AMS(), new DeploymentOptions().setConfig(new JsonObject().put("AID", amsAID.getName())));//todo configure ams aid
                future.complete();
            } else {
                logger.error("Error starting the AP " + compositeFutureAsyncResult.cause());
                future.fail(compositeFutureAsyncResult.cause());
            }
        });
    }

}
