package com.indev.radex.directoryservice.service;

import com.indev.radex.agent.AID;
import com.indev.radex.directoryservice.ServiceDescription;
import com.indev.radex.directoryservice.TransportDescription;
import com.indev.radex.directoryservice.TransportType;
import org.junit.Test;

import java.util.HashMap;

public class AgentDirectoryMatcherTest {
    private AgentDirectoryMatcher matcher = new AgentDirectoryMatcher();
    private DFDescription templateDF = new DFDescription();
    private DFDescription otherDF = new DFDescription();

    @Test(expected = DFNotMatching.class)
    public void checkNotMatching() {
        templateDF.setName(new AID("buyer"));

        otherDF.setName(new AID("seller"));

        matcher.matches(templateDF, otherDF);
    }

    @Test
    public void checkMatching() {

        templateDF.setName(new AID("buyer"));

        otherDF.setName(new AID("buyer"));

        matcher.matches(templateDF, otherDF);
    }

    @Test
    public void checkMatchingAddress() {
        AID templateAID = new AID("buyer").with(new TransportDescription(TransportType.HTTP, "http://address", new HashMap<>()));
        templateDF.setName(templateAID);

        AID otherAID = new AID("buyer").with(new TransportDescription(TransportType.HTTP, "http://address", new HashMap<>()));
        otherDF.setName(otherAID);

        matcher.matches(templateDF, otherDF);
    }

    @Test(expected = DFNotMatching.class)
    public void checkNotMatchingAddress() {
        AID templateAID = new AID("buyer").with(new TransportDescription(TransportType.HTTP, "http://address", new HashMap<>()));
        templateDF.setName(templateAID);

        AID otherAID = new AID("buyer").with(new TransportDescription(TransportType.EVENT, "http://address", new HashMap<>()));
        otherDF.setName(otherAID);

        matcher.matches(templateDF, otherDF);
    }


    @Test(expected = DFNotMatching.class)
    public void checkNotMatchingAddressDifferentSize() {
        AID templateAID = new AID("buyer")
                .with(new TransportDescription(TransportType.EVENT, "http://address", new HashMap<>()));

        templateDF.setName(templateAID);

        AID otherAID = new AID("buyer")
                .with(new TransportDescription(TransportType.HTTP, "http://address", new HashMap<>()))
                .with(new TransportDescription(TransportType.HTTP, "http://address", new HashMap<>()));
        otherDF.setName(otherAID);

        matcher.matches(templateDF, otherDF);
    }


    @Test(expected = DFNotMatching.class)
    public void checkNotMatchingAddressDifferentPosition() {
        AID templateAID = new AID("buyer")
                .with(new TransportDescription(TransportType.HTTP, "http://address", new HashMap<>()));

        templateDF.setName(templateAID);

        AID otherAID = new AID("buyer")
                .with(new TransportDescription(TransportType.EVENT, "http://address", new HashMap<>()))
                .with(new TransportDescription(TransportType.HTTP, "http://address", new HashMap<>()));
        otherDF.setName(otherAID);

        matcher.matches(templateDF, otherDF);
    }

    @Test(expected = DFNotMatching.class)
    public void checkNotMatchingServiceDifferentPosition() {
        AID templateAID = new AID("buyer")
                .with(new TransportDescription(TransportType.HTTP, "http://address", new HashMap<>()));

        templateDF.setName(templateAID);
        templateDF.withService(new ServiceDescription("buy-service").withProtocol("http"));

        AID otherAID = new AID("buyer")
                .with(new TransportDescription(TransportType.EVENT, "http://address", new HashMap<>()))
                .with(new TransportDescription(TransportType.HTTP, "http://address", new HashMap<>()));
        otherDF.setName(otherAID);

        matcher.matches(templateDF, otherDF);
    }
}