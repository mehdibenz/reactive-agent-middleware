package com.indev.radex.ams;

import io.vertx.ext.unit.TestSuite;

public class AMSTest {
    public static void test_01() {
        TestSuite suite = TestSuite.create("the_test_suite");
        suite.test("my_test_case", context -> {
            String s = "value";
            context.assertEquals("value", s);
        });
        suite.run();
    }


}